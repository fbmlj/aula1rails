Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/apresentacao/:name/:number', to: 'apresentation#home', as: "home"
  get '/comidas', to: "food#now", as: "food_now"
end
